package com.example.airportfinder.model

import android.os.Build
import androidx.annotation.RequiresApi

object SwissAirports {

    private val airportMap = mapOf(
        "Zürich" to "ZRH",
        "Samedan" to "SMV",
        "St. Gallen" to "ACH",
        "Sion" to "SIR",
        "Lugano" to "LUG",
        "Genf" to "GVA",
        "Buochs" to "BXO",
        "Bern" to "BRN",
        "Basel" to "BSL"
    )

    @RequiresApi(Build.VERSION_CODES.N)
    fun getAirportName(ort:String) : String {
        return airportMap.getOrDefault(ort, "Sorry, in dieser Ortschaft kenne ich keinen Flughafen!")
    }
}